<div class="test-css">
	<h1>Custom posts</h1>
	<?php

		$args = array('post_type' => 'movies', 'posts_per_page' => -1);

		$posts = new WP_Query($args);
		
		if($posts->have_posts()){
			while($posts->have_posts()){
				$posts->the_post();
				the_title();
				the_content();
				the_post_thumbnail('medium');
			}
		}

	?>
</div>
