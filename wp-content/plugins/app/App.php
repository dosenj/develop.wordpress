<?php

/*
Plugin Name: App Dev
Description: This is app dev plugin
Version: 0.0.1
Author: Jovan Dosen
License: GPL2
*/

if(!defined('ABSPATH')){
	exit();
}

require 'include/CustomPostType.php';
require 'include/CustomTaxonomy.php';
require 'include/AddAssets.php';
require 'include/CustomMetaBox.php';

class App

{
	public function __construct()
	{
		add_action('plugins_loaded', array($this, 'boot'));
	}

	public function boot()
	{
		$customPostType = new CustomPostType();
		$customTaxonomy = new CustomTaxonomy();
		$addAssets = new AddAssets();
		$customMetaBox = new CustomMetaBox();
	}

	public static function create()
	{
		return new self();
	}
}

App::create();

?>