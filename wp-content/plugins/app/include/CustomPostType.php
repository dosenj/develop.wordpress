<?php

class CustomPostType

{
	public function __construct()
	{
		add_action('init', array($this, 'customPostType'));
	}

	public function customPostType() 
	{
	    $labels = array(
	        'name'                => _x( 'Movies', 'Post Type General Name', 'development' ),
	        'singular_name'       => _x( 'Movie', 'Post Type Singular Name', 'development' ),
	        'menu_name'           => __( 'Movies', 'development' ),
	        'parent_item_colon'   => __( 'Parent Movie', 'development' ),
	        'all_items'           => __( 'All Movies', 'development' ),
	        'view_item'           => __( 'View Movie', 'development' ),
	        'add_new_item'        => __( 'Add New Movie', 'development' ),
	        'add_new'             => __( 'Add New', 'development' ),
	        'edit_item'           => __( 'Edit Movie', 'development' ),
	        'update_item'         => __( 'Update Movie', 'development' ),
	        'search_items'        => __( 'Search Movie', 'development' ),
	        'not_found'           => __( 'Not Found', 'development' ),
	        'not_found_in_trash'  => __( 'Not found in Trash', 'development' ),
	    );
	    $args = array(
	        'label'               => __( 'movies', 'development' ),
	        'description'         => __( 'Movie news and reviews', 'development' ),
	        'labels'              => $labels,
	        'supports'            => array( 'title', 'editor', 'thumbnail', ),
	        'taxonomies'          => array( 'genres' ),
	        'hierarchical'        => false,
	        'public'              => true,
	        'show_ui'             => true,
	        'show_in_menu'        => true,
	        'show_in_nav_menus'   => true,
	        'show_in_admin_bar'   => true,
	        'menu_position'       => 5,
	        'can_export'          => true,
	        'has_archive'         => true,
	        'exclude_from_search' => false,
	        'publicly_queryable'  => true,
	        'capability_type'     => 'page',
	    );
    	register_post_type( 'movies', $args );
	}
}

?>