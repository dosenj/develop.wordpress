<?php

class CustomMetaBox

{
	public function __construct()
	{
		add_action('add_meta_boxes', array($this, 'addBox'));
		add_action('save_post', array($this, 'saveData'));
	}

	public function addBox()
	{
	    $screens = ['post', 'movies'];
	    foreach ($screens as $screen) {
	        add_meta_box(
	            'box_id',           
	            'Custom Meta Box Title',  
	            array($this, 'addHtml'),  
	            $screen                   
	        );
	    }
	}

	public function addHtml($post)
	{
		$value = get_post_meta($post->ID, 'box_id', true);
	    ?>
	    <label for="data">Description for this field</label>
	    <input type="text" name="data" id="data" value="<?php echo $value; ?>">
	    <?php
	}

	public function saveData($post_id)
	{
	    if (array_key_exists('data', $_POST)) {
	        update_post_meta(
	            $post_id,
	            'box_id',
	            $_POST['data']
	        );
	    }
	}
}	

?>