<?php

class AddAssets

{
	public function __construct()
	{
		add_action('wp_enqueue_scripts', array($this, 'addScripts'));
	}

	public function addScripts()
	{
		wp_register_script('application', plugins_url('/app/assets/js/application.js'), array('jquery'),'1.1', true);
		wp_enqueue_script('application');

		wp_register_style('applicationcss', plugins_url('/app/assets/css/application.css'));
		wp_enqueue_style('applicationcss');
	}
}

?>