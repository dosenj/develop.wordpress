<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'aurora');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'uJNW@rRM(?n^[?>3eEm_*zY9&Qv$|4|pT$4d&eBGD/%Ja3t+^I`;Z[;9h8yk~+//');
define('SECURE_AUTH_KEY',  '%Vh=/4r6gcvp NFgc?8tsrYLyv(k?d fGv.NruL+vJ~(18VBY)@[oW:3dw$(n{aj');
define('LOGGED_IN_KEY',    '=<49xhPIQck^6739z<Oe0m9h!s+}dlpLb8v1c4d,_^+8UqgtKx:>&30C$4.6hl0R');
define('NONCE_KEY',        'e_he#Y~xwEoRIGhX[|zO(Ax1c3:~RR~*hhO|?$sL. :sO}U%P/G7r]z{6oz^r-/{');
define('AUTH_SALT',        '|q6N(9[-ZgGc5jIM%`3S#4ShGQlwDA##y/p7*;u%r)r25a_}m5<{{4%se%uqO(`z');
define('SECURE_AUTH_SALT', '=OnnP6ZhDUyx/2Je+;@u?DH)~=( Wz2]rA~i!!I?Rx+%owCse,/d1{R`VU.Z{|Bd');
define('LOGGED_IN_SALT',   'E_|nU%{NJ.r~!Y!GO;6TQo@~gMCu:>YiDzW>|rPSc&g,w9)vmD8zUI#[PnK2Z*mv');
define('NONCE_SALT',       'jALyKwyG(h)4swv)+NmXAcQg8k;pKjU7G`I} %r-:Mu.|3}:i_8j%9>{mi +d}AX');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
